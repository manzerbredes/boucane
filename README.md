# Boucane
**What is Boucane**
Boucane is a *64 bits* multi-tasking kernel for *x86* architecture written in pure *C++* and *GNU Assembly*. It was developed for popularization purpose and propagating Operating System developement knowledge.

**What I will learn**
- Deep C/assembly
- Creating a Multiboot2 compliant kernel
- Managing memory segmentation
- Handle hardware interrupts (clock, keyboard etc..)
- Managing memory paging
-  Userspace/Kernel Space
- Developping software interruptions (system calls)
- Making a scheduler
- And many tricks about the x86 architecture!

**Requirements**
- A computer running a Linux distribution
- Time/Motivation

**Memory Organization**
![Memory Organization](tools/ram.svg)

