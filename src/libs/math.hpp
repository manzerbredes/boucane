#pragma once

#include "core/types.hpp"

u32 pow(u32 x, u32 n);
u32 max(u32 x, u32 y);
u32 min(u32 x, u32 y);
u32 abs(u32 x);
