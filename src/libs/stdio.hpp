#pragma once

#include "core/types.hpp"

/// @brief Current active framebuffer driver
extern void (*__putchar)(char);

/**
 * Print a char* in the framebuffer
 */
extern "C" void printf(char *,...);

/**
 * Print a char*
 */
void print(char *s);

/**
 * Print an integer using itoa()
 */
void printi(int i);

/**
 * Print an integer as hex using itoh()
 */
void printh(u64 h);

