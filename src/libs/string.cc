#include "string.hpp"
#include "math.hpp"

void memcpy(void* src, void* dst, u64 size){
    u8 *c_src=(u8*)src;
    u8 *c_dst=(u8*)dst;
    for(u64 i=0;i<size;i++){
        *(c_dst+i)=*(c_src+i);
    }
}

void memset(void *dst, char value, u64 size){
    char* ptr=(char*)dst;
    for(u64 i=0;i<size;i++){
        ptr[i]=value;
    }
}

void itoa(u64 i, char *a){
    // Check if lower than 0
    u8 neg=0;
    if(i<0){
        neg=1;
        i=-i;
        a[0]='-';
    }

    // Count number of digits
    u32 len=1;
    u64 pow_value=i/pow(10,len);
    while(pow_value>=1)
    {
        len++;
        pow_value=i/pow(10,len);
    }

    // Build string
    u32 max_pow=len-1;
    for(u32 j=0;j<=max_pow;j++){
        u32 cur_pow=pow(10,max_pow-j);
        u8 digit=i/cur_pow;
        a[j+neg]='0'+digit;
        i=i-digit*cur_pow; // Remove first digits (most significant)
    }
    a[len+neg]='\0';
}

void itoh(u64 i, char *a){
    char hex[]={'0','1','2','3','4','5','6','7','8','9',
        'A','B','C','D','E','F'
    };

    // i should be split int two
    // indeed shifting with more than 32 bits seems undefined 
    u32 i_a=i&0xFFFFFFFF;
    u32 i_b=i>>32;

   for(u8 j=0;j<8;j++){
        u64 t=(j*4);
        u64 mask=0xF;
        mask=mask <<  t;
        u64 index=(i_a&mask) >> t;
        a[15-j]=hex[index];
    } 

   for(u8 j=0;j<8;j++){
        u64 t=(j*4);
        u64 mask=0xF;
        mask=mask <<  t;
        u64 index=(i_b&mask) >> t;
        a[15-(j+8)]=hex[index];
    }
    a[16]='\0';
}

u32 strlen(char *s){
    u32 i=0;
    while(s[i]!='\0')
        i++;
    return i;
}

void substr(u32 s, u32 e, char *src, char *dst){
    u32 size=abs(e-s)+1;
    memcpy(src+s, dst, size);
    dst[size]='\0';
}