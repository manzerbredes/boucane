#pragma once

#include "core/types.hpp"

/**
 * Copy data byte per byte from src to dst
 */
void memcpy(void *src, void *dst, u64 size);

void memset(void *dst, char value, u64 size);

/**
 * Convert int to char array
 */
void itoa(u64 i, char *a);

/**
 * Convert int to char array
 */
void itoh(u64 i, char *a);

/**
 * Length of a char*
 */
u32 strlen(char *s);

/**
 * Substr
 */
void substr(u32 s, u32 e, char *src, char *dst);
