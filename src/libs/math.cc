#include "math.hpp"

u32 pow(u32 x, u32 n) {
  if (n < 0)
    return -1;
  else if (n == 0)
    return 1;
  else if (n == 1)
    return x;
  u32 ret = x;
  for (u32 i = 0; i < (n - 1); i++)
    ret *= x;
  return ret;
}

u32 max(u32 x, u32 y) {
  if (x > y)
    return x;
  return y;
}

u32 min(u32 x, u32 y) {
  if (x < y)
    return x;
  return y;
}

u32 abs(u32 x) {
  if (x < 0)
    return -x;
  return x;
}
