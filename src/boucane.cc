#include "boucane.hpp"
#include "core/apic.hpp"
#include "core/paging.hpp"
#include "drivers/memtext.hpp"
#include "core/idt.hpp"
#include "boot/multiboot2.hpp"
#include "drivers/framebuffer.hpp"
#include "drivers/psftext.hpp"
#include "drivers/vgatext.hpp"
#include "libs/stdio.hpp"
#include "libs/string.hpp"
#include "drivers/bmp.hpp"
#include "core/scheduler.hpp"

/// @brief Temporary
#define TASK_DELAY 10000000

u64 kvar_kernel_vma;
u64 kvar_stack_pma;
u64 kvar_userspace_pma;
u64 kvar_bss_start;
u64 kvar_bss_end;
u64 kvar_terminus_psf_start;
u64 kvar_terminus_psf_end;
u64 kvar_logo_bmp_start;
u64 kvar_logo_bmp_end;
TSS kvar_tss;

void (*printk)(char *str,...)=printf;
extern u64 gdt64_tss;

void configure_tss(){
    // Get TSS physical address
    u64 tss_addr=(u64)&kvar_tss;
    u32 limit=sizeof(TSS);

    u32 desc1=(tss_addr&0xFFFF)<<16|(limit&0xFFFF);
    tss_addr>>=16;
    u32 desc2=(tss_addr&0xFF);
    desc2|=0b11101001<<8; // Type, Permission, present
    tss_addr>>=8;
    desc2|=tss_addr<<24;
    tss_addr>>=8;
    u32 desc3=tss_addr;
    
    // Configure GDT
    u32 *gdt_entry=(u32*)&gdt64_tss;
    gdt_entry[0]=desc1;
    gdt_entry[1]=desc2;
    gdt_entry[2]=desc3;
    gdt_entry[3]=0;

    // Configure segment
    kvar_tss.rsp0=(u64)VIRT(kvar_stack_pma);
    kvar_tss.iomap_address=0;
    asm(
        "mov $0x28, %ax   \n\t"
        "ltr %ax"
    );
}

void task1(){
    while(1){
        asm("mov $1, %rdi;int $0x30");
        for(u32 delay=0;delay<=TASK_DELAY;delay++){}
    }
}
void task2(){
    while(1){
        asm("mov $2, %rdi;int $0x30");
        for(u32 delay=0;delay<=TASK_DELAY;delay++){}
    }
}

void task3(){
    while(1){
        asm("mov $3, %rdi;int $0x30");
        for(u32 delay=0;delay<=TASK_DELAY;delay++){}
    }
}


extern "C" void boucane(u64 mb_info){
    // Init linker variables
    asm volatile ("movq $__kernel_vma, %0":"=m"(kvar_kernel_vma));
    asm volatile ("movq $__userspace_pma, %0":"=m"(kvar_userspace_pma));
    asm volatile ("movq $__stack_pma, %0":"=m"(kvar_stack_pma));
    asm volatile ("movq $__bss_start, %0":"=m"(kvar_bss_start));
    asm volatile ("movq $__bss_end, %0":"=m"(kvar_bss_end));
    asm volatile ("movq $res_binary_res_terminus_psf_start, %0":"=m"(kvar_terminus_psf_start));
    asm volatile ("movq $res_binary_res_terminus_psf_end, %0":"=m"(kvar_terminus_psf_end));
    asm volatile ("movq $res_binary_res_logo_bmp_start, %0":"=m"(kvar_logo_bmp_start));
    asm volatile ("movq $res_binary_res_logo_bmp_end, %0":"=m"(kvar_logo_bmp_end));

    // Init data structures
    asm volatile ("call load_gdt");
    configure_tss();
    paging_enable();
    memtext_init();
    idt_enable_interrupt();
    apic_enable();

    // Looking for framebuffer
    FRAMEBUFFER fb_info;
    if(mb2_find_framebuffer((u32*)mb_info, &fb_info)){
        if(fb_info.bpp>16){
            FB_CFG conf;
            conf.depth=fb_info.bpp;
            conf.location=fb_info.addr;
            conf.pitch=fb_info.pitch;
            conf.width=fb_info.width;
            conf.height=fb_info.height;
            framebuffer_init(conf);
            psftext_init((void*)kvar_terminus_psf_start);
            __putchar=psftext_putchar;
        }
        else{
            vgatext_init();
            __putchar=vgatext_putchar;
        }
    }
    
    // Booting!
    printk("Booting Boucane v%d.%d.%d\n",VERSION_MAJOR,VERSION_MINOR, VERSION_PATH);
    printk("System informations -- ");
    char bootloader[20];
    if(mb2_find_bootloader_name((u32*)mb_info,bootloader)){
        printk("BOOT:%s  ", bootloader);
    }
    MEM_INFO mem_infos;
    if(mb2_find_mem((u32*)mb_info,&mem_infos)){
        u64 mem=mem_infos.mem_upper-mem_infos.mem_lower;
        mem/=1024;
        printk("RAM:%dMB\n", mem);
    }
    

    printk("Launching processes... ");
    create_task((void*)task1, 100);
    create_task((void*)task2, 100);
    create_task((void*)task3, 100);
    scheduler_start();
    while(1);
}