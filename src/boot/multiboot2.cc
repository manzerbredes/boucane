#include "multiboot2.hpp"
#include "core/paging.hpp"
#include "libs/string.hpp"
#include "libs/stdio.hpp"

u32* mb2_find_tag(u32 *mb2_info_addr, char type){
    PAGE_VIRT_MAP(mb2_info_addr,PAGING_OPT_DEFAULTS);
    u32 size=(u32)VIRT(mb2_info_addr)[0]; // Todo: check for size
    PAGE_VIRT_RMAP(mb2_info_addr, PAGING_OPT_DEFAULTS,size);

    char *location=((char*)VIRT(mb2_info_addr))+8; // Goto first tag
    char *start=(char*)VIRT(mb2_info_addr);

    while((location-start) < size){
        // Goto next 64bit align address
        while(((u64)location&0x7) != 0)
            location++;
        // Parse type
        u32 cur_type=((u32*)location)[0];
        u32 cur_size=((u32*)location)[1];

        if(cur_type==type){
            return (u32*)location;
        }
        if(type > 30){
            printk("Strange multiboot infos structure...\n");
            return 0;
        }
        location+=cur_size;
    }

    return 0;
}

char mb2_find_bootloader_name(u32* mb2_info_addr, char *return_name){
    u32* addr=mb2_find_tag(mb2_info_addr,2);
    if(addr){
        u32 size=addr[1];
        memcpy(addr+2, return_name, size);
        return 1;
    }
    return 0;
}

char mb2_find_new_rsdp(u32* mb2_info_addr, u64 *return_addr, u32 *return_size){
    u32* addr=mb2_find_tag(mb2_info_addr,15);
    if(addr){
        *return_size=addr[1];
        *return_addr=(u64)(addr+2);
        return 1;
    }
    return 0;   
}

char mb2_find_old_rsdp(u32* mb2_info_addr, u64 *return_addr, u32 *return_size){
    u32* addr=mb2_find_tag(mb2_info_addr,14);
    if(addr){
        *return_size=addr[1];
        *return_addr=(u64)(addr+2);
        return 1;
    }
    return 0;   
}

char mb2_find_framebuffer(u32* mb2_info_addr, FRAMEBUFFER *fb){
    u32* addr=mb2_find_tag(mb2_info_addr,8); 
    if(addr){
        memcpy(addr, fb, sizeof(FRAMEBUFFER));
        return 1;
    }
    return 0;
}

char mb2_find_mem(u32* mb2_info_addr, MEM_INFO *mem){
    u32* addr=mb2_find_tag(mb2_info_addr,4); 
    if(addr){
        memcpy(addr, mem, sizeof(MEM_INFO));
        return 1;
    }
    return 0;
}