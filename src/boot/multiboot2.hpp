#pragma once

#include "core/types.hpp"
#include "boucane.hpp"

typedef struct TAG_HEADER {
    u32 type;
    u32 size;
}__attribute__((packed)) TAG_HEADER;

typedef struct FRAMEBUFFER {
    TAG_HEADER header;
    u64 addr;
    u32 pitch;
    u32 width;
    u32 height;
    u8 bpp;
    u8 type;
    u8 reserved;
} __attribute__((packed)) FRAMEBUFFER;

typedef struct {
    TAG_HEADER header;
    u32 mem_lower;
    u32 mem_upper;
} __attribute__((packed)) MEM_INFO;


u32* mb2_find_tag(u32 *mb2_info_addr, char type);
char mb2_find_bootloader_name(u32* mb2_info_addr, char *return_name);
char mb2_find_new_rsdp(u32* mb2_info_addr, u64 *return_addr, u32 *return_size);
char mb2_find_old_rsdp(u32* mb2_info_addr, u64 *return_addr, u32 *return_size);
char mb2_find_framebuffer(u32* mb2_info_addr, FRAMEBUFFER *fb);
char mb2_find_mem(u32* mb2_info_addr, MEM_INFO *mem);