#pragma once

#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_PATH 0

#include "core/types.hpp"
#include "core/paging.hpp"
#include "libs/math.hpp"
#include "libs/stdio.hpp"
#include "libs/string.hpp"

// ---- Debug
#define DUMP(var) asm volatile("push $0xABC; push %0; push $0xABC; _%=:; jmp _%="::"r"(var))
#define DUMP2(var1,var2) asm volatile("push $0xABC; push %0; push %1; push $0xABC; _%=:; jmp _%="::"a"(var1),"b"(var2))

// Functions
extern void (*printk)(char *str,...);

typedef struct {
    u32 reserved;
    u64 rsp0;
    u64 rsp1;
    u64 rsp2;
    u64 reserved2;
    u64 ist1;
    u64 ist2;
    u64 ist3;
    u64 ist4;
    u64 ist5;
    u64 ist6;
    u64 ist7;
    u64 reserved3;
    u16 reserved4;
    u16 iomap_address;
} __attribute__((packed)) TSS;

/// @brief Various variables from the linker
extern u64 kvar_kernel_vma;
extern u64 kvar_stack_pma;
extern u64 kvar_userspace_pma;
extern u64 kvar_bss_start;
extern u64 kvar_bss_end;
extern TSS kvar_tss;


/// @brief Binary references
extern u64 kvar_terminus_psf_start;
extern u64 kvar_terminus_psf_end;
extern u64 kvar_logo_bmp_start;
extern u64 kvar_logo_bmp_end;