#pragma once


#include "boucane.hpp"


extern "C" void apic_ack();
void apic_enable();
void apic_write(u32 reg, u32 value);
u32 apic_read(u32 reg);