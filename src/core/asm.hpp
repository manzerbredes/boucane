#pragma once

#define cli() asm("cli")

#define sti() asm("sti")

#define READ_MSR(reg,high,low) \
    asm volatile( \
        "mov %2, %%ecx;rdmsr   \n\t" \
        "mov %%edx, %0         \n\t" \
        "mov %%eax, %1" \
        : "=m" (high), "=m" (low) :"i" (reg))

#define WRITE_MSR(reg,high,low) \
    asm volatile( \
        "mov %1, %%edx  \n\t" \
        "mov %2, %%eax  \n\t" \
        "mov %0, %%ecx;wrmsr"::"i" (reg), "m" (high), "m" (low))

#define outb(port,value) \
    asm volatile ("outb %%al, %%dx" :: "a"(value), "d" (port) )

#define outbj(port,value) \
    asm volatile ("outb %%al, %%dx;" :: "a" (value), "d"(port) )

#define inb(port,dst) \
    asm volatile ("inb %%dx, %%al": "=a" (dst) : "d" (port))

#define lpml4(pml4) \
    asm volatile ("mov %0, %%rax; mov %%rax, %%cr3":: "r" (pml4))
