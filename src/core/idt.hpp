#pragma once

#include "types.hpp"
#include "libs/stdio.hpp"

#define IDT_GATE_SIZE 16
#define IDT_MAX_ENTRIES 200
#define IDT_ADDR 0

#define IDT_OPT_P (1 << 15)
#define IDT_OPT_TYPE_INT 0xE << 8
#define IDT_OPT_TYPE_TRAP 0xF << 8
#define IDT_OPT_PRVL_0 0
#define IDT_OPT_PRVL_1 (1 << 13)
#define IDT_OPT_PRVL_2 (2 << 13)
#define IDT_OPT_PRVL_3 (3 << 13)

typedef struct IDT_REGISTER {
    u16 limit;
    u64 base;
} __attribute__((packed)) IDT_REGISTER;

typedef struct IDT_DESCRIPTOR {
    u64 offset;
    u16 selector;
    u16 options;
    u8 ist;
    u32 ign;
} __attribute__((packed)) IDT_DESCRIPTOR;

/// @brief 3 u32 per entry
extern u32 idt[IDT_MAX_ENTRIES][4];

void idt_enable_interrupt(void);
void idt_write_descriptor(IDT_DESCRIPTOR desc, u16 index);