#pragma once

#include "boucane.hpp"

#define MAX_TASK 5
#define TASK_VMA 0x0
#if (TASK_VMA % 0x7)
    #error TASK_VMA value is not 4096 bits aligned!
#endif

// DO NOT CHANGE THE FOLLOWING STRUCTURE WITHOUT CONCIDERING UPDATING
// THE SWITCH FUNCTION INTO scheduler_asm.S
typedef struct {
    u64 rax, rbx, rcx, rdx;  // 8 16 24 32
    u64 cs, rip;             // 40 48
    u64 ss, rsp, rbp;        // 56 64 72
    u64 rsi, rdi;            // 80 88
    u64 ds, es, fs, gs;      // 96 104 112 120
    u64 eflags;              // 128
    u64 rsp0;                // 136
    u64 r8,r9,r10,r11,r12,r13,r14,r15; // 144 152 160 168 176 184 192 200
} __attribute__((packed)) REGS;

// DO NOT CHANGE THE FOLLOWING STRUCTURE WITHOUT CONCIDERING UPDATING
// THE SWITCH FUNCTION INTO scheduler_asm.S
typedef struct {
    u64* pml4;
    REGS registers;
    u32 id;
    u32 pid;
    u32 size;
} __attribute__((packed)) PROC;

extern char show_ticks;

extern "C" void clock();
void schedule(u64* stack);
void create_task(void*task, u32 size);
void scheduler_start();