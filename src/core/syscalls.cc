#include "boucane.hpp"
#include "core/asm.hpp"
#include "drivers/framebuffer.hpp"
#include "drivers/psftext.hpp"

extern "C" void syscall(){
    u64 call_number;
    asm volatile("mov %%rdi, %0":"=m"(call_number)::"rdi");

    cli();
    printk("%d",call_number);
    sti();
}