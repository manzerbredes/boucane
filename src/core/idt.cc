#include "idt.hpp"
#include "boucane.hpp"
#include "core/paging.hpp"
#include "libs/string.hpp"


u32 idt[IDT_MAX_ENTRIES][4] __attribute__((aligned(4096)));;
IDT_REGISTER IDTR;
extern u64 INT_DEFAULT,INT_0,INT_10,INT_14,INT_KBD,INT_CLK,INT_SYSCALL;

void idt_enable_interrupt(void){
    IDTR.base=((u64)idt);
    IDTR.limit=sizeof(idt);

    IDT_DESCRIPTOR d;
    d.ign=0;
    d.ist=0;
    d.selector=0x08;
    d.options=IDT_OPT_P|IDT_OPT_PRVL_0|IDT_OPT_TYPE_INT;

    // Write idt entries
    for(u16 i=0;i<IDT_MAX_ENTRIES;i++){
        if(i==0){ // Zero-division
            d.offset=(u64)&INT_0;
            idt_write_descriptor(d, i);
        }
        else if(i==14){ // Page fault
            d.offset=(u64)&INT_14;
            idt_write_descriptor(d, i);
        }
        else if(i==10){ // TSS
            d.offset=(u64)&INT_10;
            idt_write_descriptor(d, i);
        }
        else if(i==60){ // Keyboard
            d.offset=(u64)&INT_KBD;
            idt_write_descriptor(d, i);
        }
        else if(i==61){ // Clock
            d.offset=(u64)&INT_CLK;
            idt_write_descriptor(d, i);
        }
        else if(i==0x30){ // Syscall
            IDT_DESCRIPTOR d2;
            d2.ign=0;
            d2.ist=0;
            d2.selector=0x08;
            d2.options=IDT_OPT_P|IDT_OPT_PRVL_3|IDT_OPT_TYPE_TRAP;
            d2.offset=(u64)&INT_SYSCALL;
            idt_write_descriptor(d2, i);
        }
        else {
            d.offset=(u64)&INT_DEFAULT;
            idt_write_descriptor(d, i);
        }
    }  
    // Enable interrupts
    asm(
        "lidt (IDTR)  \n\t"
        "sti          \n\t ");
}

void idt_write_descriptor(IDT_DESCRIPTOR desc, u16 index){
    u32 desc0_31=(desc.selector << 16) | desc.offset&0xFFFF;
    u32 desc32_63=desc.ist | desc.options | (desc.offset&0xFFFF0000);
    u32 desc64_95=desc.offset>>32;
    u32 desc96_127=desc.ign;
    idt[index][0]=desc0_31;
    idt[index][1]=desc32_63;
    idt[index][2]=desc64_95;
    idt[index][3]=desc96_127;
}