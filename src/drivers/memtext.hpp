#pragma once

#include "boucane.hpp"

#define MEMTEXT_BUFFER_SIZE 4096
#define MEMTEXT_ADDR_LOCATION 0x0


void memtext_init();                        
void memtext_putchar(char c);
void memtext_scrollup(u32 n);
