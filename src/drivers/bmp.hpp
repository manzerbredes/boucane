#pragma once

#include "boucane.hpp"

typedef struct {
    u16 signature;
    u32 filesize;
    u32 reserved;
    u32 data_offset;
    u32 info_header_size;
    u32 width;
    u32 height;
    u16 planes;
    u16 bpp;
    u32 compression;
} __attribute__((packed)) BMP_HEADER;

void bmp_draw(u8* bmp_data, int x, int y);