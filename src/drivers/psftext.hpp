#pragma once

#include "boucane.hpp"

#define PSF_MAGIC 0x864ab572

typedef struct PSF_HEADER {
    u32 magic;
    u32 version;
    u32 header_length;
    u32 flags;
    u32 glyph_count;
    u32 glyph_size;
    u32 glyph_height;
    u32 glyph_width;
} __attribute__((packed)) PSF_HEADER;

typedef struct PSF_STATUS {
    PSF_HEADER header;
    u32 x,y;
    u32 nline;
    u32 nchar;
    u8 bg,fg;
    u8* psf_addr;
} __attribute__((packed)) PSF_STATUS;


extern PSF_HEADER psf_header;

void psftext_init(void* psf_addr);
void psftext_putchar(char c);