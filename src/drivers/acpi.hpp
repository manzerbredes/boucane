#pragma once

#include "boucane.hpp"

#define ACPI_RSDP_SIGNATURE 0x2052545020445352
#define ACPI_RSDT_SIGNATURE 0x54445352
#define ACPI_MADT_SIGNATURE 0x43495041

typedef struct RSDP {
    u64 signature;
    u8 checksum;
    u64 oemid:48;
    u8 revision;
    u32 rsdt_addr;
    u32 length;
} __attribute__((packed)) RSDP;

typedef struct ACPI_TABLE_HEADER {
    u32 signature;
    u32 length;
    u8 revision;
    u8 checksum;
    u64 oemid:48;
    u64 oem_table_id;
    u32 oem_revision;
    u32 creator_id;
    u32 creator_revision;
} __attribute__((packed)) ACPI_TABLE_HEADER;

typedef struct RSDT {
    ACPI_TABLE_HEADER header;
    u32 first_entry_addr_ptr;
} __attribute__((packed)) RSDT;

typedef struct INT_CTRL_HEADER {
    u8 type;
    u8 length;
} __attribute__((packed)) INT_CTRL_HEADER;

typedef struct IOAPIC {
    INT_CTRL_HEADER header;
    u8 ioapic_id;
    u8 reserved;
    u32 ioapic_addr;
    u32 global_system_interrupt_base;
} __attribute__((packed)) IOAPIC;


extern RSDP rsdp;
extern RSDT rsdt;
extern IOAPIC ioapic;

char acpi_checksum(void* p, char size);
char acpi_load_rsdt();
char acpi_load_madt();
char acpi_init(void* rsdp_p);

