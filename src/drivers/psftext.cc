#include "psftext.hpp"
#include "core/paging.hpp"
#include "libs/string.hpp"
#include "drivers/framebuffer.hpp"

PSF_STATUS psf_status;

void psftext_init(void* psf_addr){
    printk("Loading PSF font... ");
    memcpy(psf_addr, &psf_status.header, sizeof(PSF_HEADER));
    if(psf_status.header.magic!=PSF_MAGIC){
        printk("Invalid PSF magic number. Abort.\n");
        return;
    }

    printk("Flags %d Version %d Glyphs number %d Size %dx%d", 
    psf_status.header.flags, psf_status.header.version,
    psf_status.header.glyph_count, psf_status.header.glyph_width, psf_status.header.glyph_height);
    psf_status.x=0;
    psf_status.y=0;
    psf_status.psf_addr=(u8*)psf_addr;
    psf_status.nline=fb_cfg.height/psf_status.header.glyph_height;
    psf_status.nchar=fb_cfg.width/psf_status.header.glyph_width;
    psf_status.bg=0;
    psf_status.fg=200;
    print("\n");
}

void psftext_putchar(char c){
    if(c=='\n'){
        psf_status.y++;
        psf_status.x=0;
        if(psf_status.y>=psf_status.nline){
            framebuffer_scrollup(psf_status.header.glyph_height);
            psf_status.y--;
        }
        return;
    }

    u8* glyph=(psf_status.psf_addr+psf_status.header.header_length+c*psf_status.header.glyph_size);
    FB_PIXEL pixel;
    for(int i=0;i<psf_status.header.glyph_height;i++){
        for(int k=0;k<(psf_status.header.glyph_width/8);k++){
            u8 mask=1<<7;
            u8 row=*(glyph+k);
            for(int j=0;j<8;j++){
                u16 status=row&mask;
                u8 color=status ? psf_status.fg:psf_status.bg;
                pixel.x=j+k*8+psf_status.x*psf_status.header.glyph_width;
                pixel.y=i+psf_status.y*psf_status.header.glyph_height;
                pixel.r=color;
                pixel.g=color;
                pixel.b=color;
                pixel.a=color;
                framebuffer_draw(pixel);
                mask>>=1;
            }
        }
        glyph+=psf_status.header.glyph_width/8;
    }

    psf_status.x++;
    if(psf_status.x>psf_status.nchar){
        psf_status.y++;
        psf_status.x=0;
        if(psf_status.y>=psf_status.nline){
            framebuffer_scrollup(psf_status.header.glyph_height);
            psf_status.y--;
        }
    }
}