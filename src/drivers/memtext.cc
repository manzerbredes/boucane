#include "memtext.hpp"
#include "core/paging.hpp"
#include "core/types.hpp"
#include "libs/string.hpp"


char memtext_buffer[MEMTEXT_BUFFER_SIZE];
u64 memtext_x=0;

void memtext_init(){
    PAGE_VIRT_MAP(MEMTEXT_ADDR_LOCATION,PAGING_OPT_DEFAULTS);
    u64* p_addr=(u64*)VIRT(MEMTEXT_ADDR_LOCATION);
    *p_addr=(u64)memtext_buffer;

    // Cleaning buffer
    for(memtext_x=0;memtext_x<MEMTEXT_BUFFER_SIZE;memtext_x++)
        memtext_buffer[memtext_x]=0;
    memtext_x=0;
}

void memtext_putchar(char c){
    
    if(memtext_x>=MEMTEXT_BUFFER_SIZE){
        memtext_scrollup(1);
        memtext_buffer[memtext_x-1]=c;
        return;
    }
    memtext_buffer[memtext_x]=c;
    memtext_x++;
}

void memtext_scrollup(u32 n){
    u64 start=(u64)memtext_buffer;    
   for(u64 i=0;i<MEMTEXT_BUFFER_SIZE;i++){
        if(i+n<MEMTEXT_BUFFER_SIZE)
            memtext_buffer[i]=memtext_buffer[i+n];
        else
            memtext_buffer[i]=0;
   }
}