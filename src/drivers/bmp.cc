#include "bmp.hpp"
#include "libs/string.hpp"

#include "drivers/framebuffer.hpp"

void bmp_draw(u8* bmp_data, int posx, int posy){
    BMP_HEADER header;
    memcpy(bmp_data, &header, sizeof(BMP_HEADER));
    if(header.signature!=0x4d42){
        printk("Invalid BMP data\n");
        return;
    }

    // Do not forget, each row is 32bits aligned
    u32 Bpp=header.bpp/8;
    u32 lineW=header.width*Bpp;
    while((lineW&0x3)!=0){
        lineW++;
    }
   
    for(u32 y=0;y<header.height;y++){
        for(u32 x=0;x<header.width;x++){
            u32 pos=x*Bpp+y*lineW;
            u8 *pixel=((u8*)bmp_data)+header.data_offset+pos;
            FB_PIXEL p;
            p.x=posx+x;
            p.y=posy+(header.height-y);
            p.r=pixel[0];
            p.g=pixel[1];
            p.b=pixel[2];
            p.a=0;
            framebuffer_draw(p);
            
        }
     
    }

}