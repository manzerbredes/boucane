#pragma once

#include "boucane.hpp"

typedef struct {
    u32 pitch;
    u32 depth;
    u64 location;
    u32 width;
    u32 height;
} __attribute__((packed)) FB_CFG;

typedef struct {
    u32 x,y;
    u8 r,g,b,a;
} __attribute__((packed)) FB_PIXEL;

extern FB_CFG fb_cfg;

void framebuffer_init(FB_CFG config);

void framebuffer_draw(FB_PIXEL p);

void framebuffer_scrollup(u32 npixel);

void framebuffer_clear();