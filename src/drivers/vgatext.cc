#include "vgatext.hpp"

#include "boucane.hpp"
#include "core/paging.hpp"

#define MAX_COL 80
#define MAX_LINE 25

VIDEO_STATE VS={
  (u8 *)0,
  0,
  0,
  BLACK,
  GRAY,
};

void vgatext_init(){
  PAGE_VIRT_RMAP(0xB8000,PAGING_OPT_DEFAULTS,2);
  VS.mem=(u8*)VIRT(0xB8000);
}

void vgatext_putchar(char c){
  // Handle newline here
  if(c=='\n'){
    VS.col=0;
    VS.line+=1;
    if(VS.line>=MAX_LINE){
      VS.line=MAX_LINE-1;
      vgatext_scrollup();
    }
    return;
  }
  
  // Print char
  VS.mem[VS.col*2+MAX_COL*VS.line*2]=c;
  VS.mem[VS.col*2+MAX_COL*VS.line*2+1]=VS.fg|VS.bg<<4;
  
  // Refresh location
  VS.col+=1;
  if(VS.col>= MAX_COL){
    VS.col=0;
    VS.line+=1;
    if(VS.line>=MAX_LINE){
      VS.line=MAX_LINE-1;
      vgatext_scrollup();
    }
  }
}

void vgatext_clear(){
  for(u8 i=0;i<MAX_LINE;i++){
    vgatext_scrollup();
  }
}

void vgatext_scrollup(){
  // Move VS.line up
  for(u8 i=1;i<=MAX_LINE;i++){
    for(u8 j=0;j<=MAX_COL;j++)
      VS.mem[j*2+MAX_COL*(i-1)*2]=VS.mem[j*2+MAX_COL*i*2];
  }
  // Clear last VS.line
  for(u8 i=0;i<=MAX_COL;i++){
    VS.mem[i*2+MAX_COL*(MAX_LINE-1)*2]='\0';
  }
}
