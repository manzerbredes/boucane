#pragma once

#include "core/types.hpp"

typedef enum VIDEO_COLORS {
  BLACK=0, BLUE=1, GREEN=2,CYAN=3, RED=4,PURPLE=5,BROWN=6,GRAY=7,
  DARK_GRAY=8,LIGHT_BLUE=9,LIGHT_GREEN=10,LIGHT_CYAN=11,LIGHT_RED=12,LIGHT_PURPLE=13,YELLOW=14,WHITE=15
  
} VIDEO_COLORS;

typedef struct VIDEO_STATE {
  u8 *mem;
  u8 col;
  u8 line;
  u8 bg;
  u8 fg;
} VIDEO_STATE;

void vgatext_init();

/**
 * Print char
 */
void vgatext_putchar(char);

/**
 * Scroll the framebuffer from one line
 */
void vgatext_scrollup();

/**
 * Clear all char from the framebuffer
 */
void vgatext_clear();

