#include "framebuffer.hpp"
#include "core/paging.hpp"
#include "libs/string.hpp"

FB_CFG fb_cfg;


void framebuffer_init(FB_CFG config){
    fb_cfg=config;
    // Map buffer to the end of the memory 
    // indeed fb_cfg.location could be to big and
    // thus leading to cross u64 size limit while summing it with
    // kvar_kernel_vma in paging.cc/hpp
    u64 start=0xFFFFFFFFFFFFFFFF - (fb_cfg.pitch*fb_cfg.height);
    // Ensure we start writing at the begining of the page since
    // start is not necessarly 4096 bytes aligned
    start=PAGE(start);
    PAGE_RMAP(start,fb_cfg.location, PAGING_OPT_PCD|PAGING_OPT_DEFAULTS,fb_cfg.pitch*fb_cfg.height);
    fb_cfg.location=start;
}

void framebuffer_draw(FB_PIXEL p){
    // Check overflow
    p.x=p.x>(fb_cfg.width)?fb_cfg.width:p.x;
    p.y=p.y>(fb_cfg.width)?fb_cfg.width:p.y;
    p.x=p.x<0?0:p.x;
    p.y=p.y<0?0:p.y;

    u8 *pixel=(u8*)(fb_cfg.location+p.x*(fb_cfg.depth/8)+p.y*fb_cfg.pitch);
    pixel[0]=p.r;
    pixel[1]=p.g;
    pixel[2]=p.b;
    if(fb_cfg.depth==32)
        pixel[3]=p.a;
}

void framebuffer_scrollup(u32 npixel){
    for(u32 y=0;y<fb_cfg.height;y++){
        if(npixel<fb_cfg.height){
            for(u32 x=0;x<fb_cfg.width;x++){
                u8 *pixel_dst=(u8*)(fb_cfg.location+x*(fb_cfg.depth/8)+y*fb_cfg.pitch);
                u8 *pixel_src=(u8*)(fb_cfg.location+x*(fb_cfg.depth/8)+npixel*fb_cfg.pitch);
                pixel_dst[0]=pixel_src[0]; 
                pixel_dst[1]=pixel_src[1]; 
                pixel_dst[2]=pixel_src[2]; 
                if(fb_cfg.depth==32)
                    pixel_dst[3]=pixel_src[3]; 

            }
        }
        else{
            for(u32 x=0;x<fb_cfg.width;x++){
                u8 *pixel_dst=(u8*)(fb_cfg.location+x*(fb_cfg.depth/8)+y*fb_cfg.pitch);
                *pixel_dst=0;// Faster than writing pixel by pixel
            }
        }
        npixel++;
    }
}

void framebuffer_clear(){
   // memset((void*)fb_cfg.location, 0, fb_cfg.pitch*fb_cfg.height-1000);
}